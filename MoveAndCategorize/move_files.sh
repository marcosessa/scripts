#!/bin/bash
# Questo script legge un file csv e sposta i file in base alle categorie
# Uso: bash move_files.sh input.csv

# Controlla se il file csv è fornito come argomento
if [ $# -eq 0 ]; then
  echo "Devi fornire il file csv come argomento"
    exit 1
fi

# Controlla se il file csv esiste e ha il formato corretto
csv_file="$1"
if [ ! -f "$csv_file" ]; then
  echo "Il file csv $csv_file non esiste"
    exit 2
fi

if [ ${csv_file##*.} != "csv" ]; then
  echo "Il file $csv_file non è un file csv"
    exit 3
fi

# Legge il file csv e sposta i file nelle cartelle appropriate (in base a categoria e sottocategoria)
input_dir="/volumeUSB1/usbshare/SOURCE"
output_dir="/volumeUSB1/usbshare/DESTINATION"
log_file="move_files.log"
not_found_file="not_found.csv"

# Crea il file csv con i dati dei file non trovati
echo "Filename;Category;SubCategory" > "$not_found_file"
while IFS=";" read -r filename category subcategory; do
	# Salta la prima riga con le intestazioni
	if [ "$filename" == "Filename" ]; then
		continue
	fi

	# Controlla se il file esiste nella cartella di input
	input_file="$input_dir/$filename"
	if [ ! -f "$input_file" ]; then
		echo "Il file $input_file non esiste" >> "$log_file"

		# Scrive i dati del file non trovato nel file csv
		echo "$filename;$category;$subcategory" >> "$not_found_file"
		continue
	fi

	# Crea la cartella di output se non esiste
	output_folder="$output_dir/$category/$subcategory"
	mkdir -p "$output_folder"

	# Sposta il file nella cartella di output
	output_file="$output_folder/$filename"
	mv "$input_file" "$output_file"
	echo "Spostato il file $input_file in $output_file"
	done < "$csv_file"
